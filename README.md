# Finalní projekt pro Glami

## Testované prostředí
 - Debian 11 Buster

## Prekvizity:
 - docker.io
 - docker-compose
 - python3.9
 - git

```
apt update && apt upgrade
apt install docker.io docker-compose python3.9 git -y
```

### Využítí připravených souborů
```
cd /folder/i/want
git clone git@gitlab.com:michaldudekit/glami-test.git
```

### Příprava docker volumes pro konfigurační soubory
#### Prometheus
```
docker volume create --driver local \
    --opt type=none \
    --opt device=/var/docker/glami-test/prometheus/config \
    --opt o=bind prometheus-config
```
#### Grafana
```
docker volume create --driver local \
    --opt type=none \
    --opt device=/var/docker/glami-test/grafana/config \
    --opt o=bind grafana-prom-config
```
#### Alertmanager
```
docker volume create --driver local \
    --opt type=none \
    --opt device=/var/docker/glami-test/alertmanager/config \
    --opt o=bind alertmanager-config
```

## Login do Grafany
Lze nalézt v konfiguračním souboru Grafany
Username: `michal`
Password: `Glami123`
